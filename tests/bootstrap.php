<?php

namespace {

    if ( !($loader = include __DIR__.'/../vendor/autoload.php')) {
        die(
        <<<EOT
You need to install the project dependencies using Composer:
$ wget http://getcomposer.org/composer.phar
OR
$ curl -s https://getcomposer.org/installer | php
$ php composer.phar install --dev
$ phpunit
EOT
        );
    }

    $loader->add('Rodacker\Cart\Tests', __DIR__);
}

namespace Rodacker\Cart\Identifier {


    function session_status()
    {
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ($step['function'] === 'setUp'
                && ( $step['class'] === "Rodacker\Cart\Test\Identifier\SessionTest" ||
                    $step['class'] === "Rodacker\Cart\Test\CartTest"
                )
            ) {
                return PHP_SESSION_NONE;
            }
        }

        return PHP_SESSION_ACTIVE;
    }

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}

namespace Rodacker\Cart\Storage {

    function session_status()
    {
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ($step['function'] === 'setUp'
                && $step['class']
                == "Rodacker\Cart\Test\Storage\SessionStorageTest"
            ) {
                return PHP_SESSION_NONE;
            }
        }

        return PHP_SESSION_ACTIVE;
    }

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}

namespace Rodacker\Cart {

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}