<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 22:40
 */

namespace Rodacker\CartBundle\Event;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Item\CartItem;
use Symfony\Component\EventDispatcher\Event;

class CartEvent extends Event
{
    /** @var  CartInterface */
    private $cart;

    /**
     * CartEvent constructor.
     *
     * @param CartInterface $cart
     */
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return CartInterface
     */
    public function getCart()
    {
        return $this->cart;
    }
}