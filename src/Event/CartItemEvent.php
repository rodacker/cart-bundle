<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 22:40
 */

namespace Rodacker\CartBundle\Event;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Item\CartItem;

class CartItemEvent extends CartEvent
{

    /** @var  CartItem */
    private $item;

    /**
     * CartItemEvent constructor.
     *
     * @param CartInterface $cart
     * @param CartItem      $item
     */
    public function __construct(CartInterface $cart, CartItem $item)
    {
        parent::__construct($cart);
        $this->item = $item;
    }

    /**
     * @return CartItem
     */
    public function getItem()
    {
        return $this->item;
    }
}