<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 22:31
 */

namespace Rodacker\CartBundle\Event;

/**
 * Class CartEvents
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\CartBundle\Event
 */
final class CartEvents
{

    /**
     * the ITEM_ADDED_TO_CART event occurs after the item has been added to the cart
     * and the cart storage has been updated
     *
     * this event gives you access to the cart and the cart item
     *
     * @Event("Rodacker\CartBundle\Event\CartItemEvent")
     */
    const ITEM_ADDED_TO_CART = 'rodacker_cart.item_added_to_cart';

    /**
     * the ITEM_REMOVED_FROM_CART event occurs after the item has been removed from the cart
     * and the cart storage has been updated
     *
     * this events gives you access to the cart
     *
     * @Event("Rodacker\CartBundle\Event\CartEvent")
     */
    const ITEM_REMOVED_FROM_CART = 'rodacker_cart.item_removed_from_cart';

    /**
     * the CART_CLEARED event occurs after the cart has been cleard of all items
     * and the cart storage has been updated
     *
     * @Event("Rodacker\CartBundle\Event\CartEvent")
     */
    const CART_CLEARED = 'rodacker_cart.cart_cleared';

    /**
     * the CART_CHECKOUT event occurs in the CartController::checkout() function
     *
     * you get access to the cart in the checkout function and can set a redirect response to override the redirect route
     *
     * @Event("Rodacker\CartBundle\Event\CartSetResponseEvent")
     */
    const CART_CHECKOUT  = 'rodacker_cart.cart_checkout';
}