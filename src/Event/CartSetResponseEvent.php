<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 22:40
 */

namespace Rodacker\CartBundle\Event;

use Rodacker\Cart\CartInterface;
use Symfony\Component\HttpFoundation\Response;

class CartSetResponseEvent extends CartEvent
{

    /** @var  Response $response */
    private $response;

    /**
     * CartEvent constructor.
     *
     * @param CartInterface $cart
     */
    public function __construct(CartInterface $cart)
    {
        parent::__construct($cart);
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }
}