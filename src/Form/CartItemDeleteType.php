<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 07.06.17
 * Time: 21:10
 */

namespace Rodacker\CartBundle\Form;

use Rodacker\Cart\CartInterface;
use Rodacker\CartBundle\Form\DataTransformer\CartItemToIdTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CartItemDeleteType
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\CartBundle\Form
 */
class CartItemDeleteType extends AbstractType
{

    /** @var  CartInterface */
    private $cart;

    /**
     * CartItemDeleteType constructor.
     *
     * @param CartInterface $cart
     */
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('item', HiddenType::class, []);

        $builder->get('item')
            ->addModelTransformer(
                new CartItemToIdTransformer($this->cart)
            );
    }
}