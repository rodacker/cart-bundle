<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 07.06.17
 * Time: 21:50
 */

namespace Rodacker\CartBundle\Form\DataTransformer;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Item\CartItemInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CartItemToIdTransformer implements DataTransformerInterface
{

    /** @var  CartInterface */
    private $cart;

    /**
     * CartItemToHashidTransformer constructor.
     *
     * @param CartInterface $cart
     */
    public function __construct(CartInterface $cart)
    {
        $this->cart = $cart;
    }

    /**
     * Transforms an CartItemInterface to a hashid
     *
     * @param  CartItemInterface|null $item
     *
     * @return string
     */
    public function transform($item)
    {
        if (null === $item) {
            return '';
        }

        return $item->getId();
    }

    /**
     * get the item by the id
     *
     * @param  string $id
     *
     * @return CartItemInterface|null
     * @throws TransformationFailedException if image is not found.
     */
    public function reverseTransform($id)
    {
        if ( !$id) {
            return null;
        }

        /** @var CartItemInterface $item */
        $item = $this->cart->getItem($id);

        if (null === $item) {
            throw new TransformationFailedException(
                sprintf('An item with id "%s" does not exist!', $id)
            );
        }

        return $item;
    }
}