<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 13.06.17
 * Time: 10:15
 */

namespace Rodacker\CartBundle\Exception;

class InvalidCartClassException extends \Exception
{

}