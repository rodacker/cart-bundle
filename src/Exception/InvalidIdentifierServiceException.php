<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 13.06.17
 * Time: 14:51
 */

namespace Rodacker\CartBundle\Exception;

class InvalidIdentifierServiceException extends \Exception
{

}