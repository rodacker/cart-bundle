<?php

namespace Rodacker\CartBundle\EventListener;

use Rodacker\CartBundle\Event\CartEvent;
use Rodacker\CartBundle\Event\CartEvents;
use Rodacker\CartBundle\Event\CartItemEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;

class FlashListener implements EventSubscriberInterface
{

    /**
     * @var Session
     */
    private $session;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * FlashListener constructor.
     *
     * @param Session             $session
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Session $session,
        TranslatorInterface $translator
    ) {
        $this->session    = $session;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            CartEvents::ITEM_ADDED_TO_CART     => 'itemAdded',
            CartEvents::ITEM_REMOVED_FROM_CART => 'itemRemoved',
            CartEvents::CART_CLEARED           => 'cleared',
        ];
    }

    /**
     * @param CartItemEvent $event
     * @param string        $eventName
     */
    public function itemAdded(CartItemEvent $event, $eventName)
    {
        $item = $event->getItem();
        $this->session->getFlashBag()->add(
            'success',
            sprintf(
                'item %s has been added to the cart',
                $item->getName()
            )
        );
    }

    /**
     * @param CartItemEvent $event
     * @param string        $eventName
     */
    public function itemRemoved(CartItemEvent $event, $eventName)
    {
        $item = $event->getItem();
        $this->session->getFlashBag()->add(
            'success',
            sprintf(
                'item %s has been removed from the cart',
                $item->getName()
            )
        );
    }

    /**
     * @param CartEvent $event
     * @param string    $eventName
     */
    public function cleared(CartEvent $event, $eventName)
    {
        $this->session->getFlashBag()->add(
            'success',
            'the cart has been cleared successfully'
        );
    }

}
