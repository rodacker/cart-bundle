<?php

namespace Rodacker\CartBundle\DependencyInjection;

use Rodacker\Cart\Cart;
use Rodacker\CartBundle\Exception\InvalidIdentifierServiceException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class RodackerCartExtension extends Extension
{

    /**
     * {@inheritdoc}
     *
     * @throws InvalidIdentifierServiceException
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        // load the default service definitions
        $loader = new Loader\YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yml');

        // inject identifier from config
        if ($container->hasDefinition($config['identifier_service'])) {

            $identifier = $container->getDefinition(
                $config['identifier_service']
            );
            $class      = $identifier->getClass();
            $reflClass  = new \ReflectionClass($class);

            if ( !$reflClass->implementsInterface(
                'Rodacker\\Cart\\Identifier\\CartIdentifierInterface'
            )
            ) {
                throw new InvalidIdentifierServiceException(
                    sprintf(
                        'the identifier service %s must implement the CartIdentifierInterface interface',
                        $config['identifier_service']
                    )
                );
            }

            $cart = $container->getDefinition(Cart::class);
            $cart->replaceArgument(0, $identifier);
        }

        // set config vars as parameters
        $container->setParameter('rodacker.cart.class', $config['cart_class']);
        $container->setParameter('rodacker.cart.checkout_redirect_route', $config['checkout_redirect_route']);
    }
}
