<?php

namespace Rodacker\CartBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('rodacker_cart');

        $rootNode
            ->children()
                ->scalarNode('identifier_service')->defaultValue('rodacker_cart.cart_identifier.session_identifier')->cannotBeEmpty()->end()
                ->scalarNode('cart_class')->defaultValue('Rodacker\\CartBundle\\Cart\\Cart')->cannotBeEmpty()->end()
                ->scalarNode('checkout_redirect_route')->defaultValue('checkout_index')->cannotBeEmpty()->end()
            ->end();

        $this->addServiceSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addServiceSection(ArrayNodeDefinition $node)
    {
        $node
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('service')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('identifier')->defaultValue('rodacker.cart.session_identifier')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
