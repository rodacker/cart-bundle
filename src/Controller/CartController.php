<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 06.06.17
 * Time: 22:40
 */

namespace Rodacker\CartBundle\Controller;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Item\CartItem;
use Rodacker\CartBundle\Event\CartEvent;
use Rodacker\CartBundle\Event\CartEvents;
use Rodacker\CartBundle\Event\CartItemEvent;
use Rodacker\CartBundle\Event\CartSetResponseEvent;
use Rodacker\CartBundle\Form\CartItemDeleteType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CartController
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package AppBundle\Controller\Web
 *
 * @Route("/cart")
 */
class CartController extends Controller
{

    /**
     * @Route("", name="cart_show")
     * @Method("GET")
     */
    public function showAction(Request $request)
    {

        /** @var CartInterface $cart */
        $cart = $this->get('Rodacker\Cart\Cart');

        // get forms
        $deleteForm = $this->createDeleteForm();
        $clearForm  = $this->createClearForm();

        return $this->render(
            '@RodackerCart/Cart/show.html.twig',
            [
                'cart' => $cart,
                'deleteForm' => $deleteForm,
                'clearForm' => $clearForm->createView(),
            ]
        );
    }

    /**
     * @Route("/", name="cart_add_item")
     *
     * @Method("POST")
     */
    public function addItemAction(CartItem $item)
    {
        $cart = $this->get('Rodacker\Cart\Cart');
        $cart->addItem($item);

        // dispatch event
        $this->get('event_dispatcher')->dispatch(
            CartEvents::ITEM_ADDED_TO_CART,
            new CartItemEvent($cart, $item)
        );

        return $this->redirectToRoute('cart_show');
    }

    /**
     * @Route("/item/", name="cart_remove_item")
     * @Method("DELETE")
     */
    public function removeItemAction(Request $request)
    {
        $form = $this->createDeleteForm();
        $form->handleRequest($request);

        if ($form->isValid()) {

            /** @var CartItem $item */
            $item = $form->getData()['item'];

            $cart = $this->get('Rodacker\Cart\Cart');
            $cart->removeItem($item);

            // dispatch event
            $this->get('event_dispatcher')->dispatch(
                CartEvents::ITEM_REMOVED_FROM_CART,
                new CartItemEvent($cart, $item)
            );

            return $this->redirectToRoute('cart_show');

        } else {
            throw new NotFoundHttpException(
                sprintf('delete cart item form is not valid')
            );
        }
    }

    /**
     * @Route("/checkout", name="cart_checkout")
     * @Method("GET")
     */
    public function checkoutFunction()
    {
        $cart = $this->get('Rodacker\Cart\Cart');

        if ($cart->isEmpty()) {
            throw new HttpException(Response::HTTP_NOT_FOUND, 'cart is empty');
        }

        /** @var CartSetResponseEvent $event */
        $event = new CartSetResponseEvent($cart);
        $this->get('event_dispatcher')->dispatch(
            CartEvents::CART_CHECKOUT,
            $event
        );

        if (null === $response = $event->getResponse()) {
            $response = new RedirectResponse($this->generateUrl($this->getParameter('rodacker.cart.checkout_redirect_route')));
        }

        return $response;

    }

    /**
     * @Route("/", name="cart_clear")
     * @Method("DELETE")
     */
    public function clearFunction()
    {
        $cart = $this->get('Rodacker\Cart\Cart');
        $cart->clear();

        // dispatch event
        $this->get('event_dispatcher')->dispatch(
            CartEvents::CART_CLEARED,
            new CartEvent($cart)
        );

        return $this->redirectToRoute('cart_show');
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm()
    {
        return $this->createForm(
            CartItemDeleteType::class,
            null,
            [
                'action' => $this->generateUrl('cart_remove_item'),
                'method' => 'DELETE',
            ]
        );
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    private function createClearForm()
    {
        return $this->createFormBuilder(
            [],
            [
                'action' => $this->generateUrl('cart_clear'),
                'method' => 'DELETE',
            ]
        )->getForm();
    }
}