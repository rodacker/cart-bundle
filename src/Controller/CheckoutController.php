<?php

namespace Rodacker\CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class CheckoutController
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\CartBundle\Controller
 *
 * @Route("/")
 *
 */
class CheckoutController extends Controller
{
    /**
     * @Route("/checkout-target-page", name="checkout_index")
     */
    public function indexAction()
    {
        return $this->render('RodackerCartBundle:Checkout:index.html.twig', array(
            // ...
        ));
    }

}
