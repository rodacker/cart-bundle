<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 10:52
 */

namespace Rodacker\CartBundle\Identifier;

use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\Cart\Utils\Helper;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class SessionAttributeIdentifier
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class SessionAttributeIdentifier implements CartIdentifierInterface
{

    /** @var  Session */
    private $session;

    /**
     * SessionKeyIdentifier constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $identifier = Helper::generateIdentifier();

        $this->session->set(Helper::IDENTIFIER_KEY, $identifier);
        $this->session->save();

        return $identifier;
    }

    /**
     * @return string
     */
    public function get()
    {
        if ( !$this->session->has(
            Helper::IDENTIFIER_KEY
        )
        ) {
            return $this->generate();
        }

        return $this->session->get(
            Helper::IDENTIFIER_KEY
        );
    }

    /**
     * clears the entry by setting it to null
     */
    public function clear()
    {
        $this->session->set(Helper::IDENTIFIER_KEY, null);
        $this->session->save();
    }

}