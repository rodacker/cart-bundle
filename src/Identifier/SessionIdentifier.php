<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 10:52
 */

namespace Rodacker\CartBundle\Identifier;

use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Symfony\Component\HttpFoundation\Session\Session;

class SessionIdentifier implements CartIdentifierInterface
{

    /** @var  Session */
    private $session;

    /**
     * SessionKeyIdentifier constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function generate()
    {
        if (!$this->session->isStarted()) {
            $this->session->start();
        }

        return $this->get();
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->session->getId();
    }

    public function clear()
    {
        // we do not need to do anything as the session id is used as identifier
    }

}