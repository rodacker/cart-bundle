<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 02.06.17
 * Time: 00:51
 */

namespace Rodacker\CartBundle\Twig;

use Money\Currencies\ISOCurrencies;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;

class MoneyExtension extends \Twig_Extension
{

    public function getFilters() {
        return [
            new \Twig_SimpleFilter('money', [$this, 'moneyFilter'])
        ];
    }

    public function moneyFilter(Money $money) {
        $currencies = new ISOCurrencies();

        $numberFormatter = new \NumberFormatter('de_DE', \NumberFormatter::CURRENCY);
        $moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);

        return $moneyFormatter->format($money);
    }

}