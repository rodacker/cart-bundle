<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 10:46
 */

namespace Rodacker\CartBundle\Storage;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Exception\CanNotLoadCartException;
use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\Cart\Storage\CartStorageInterface;
use Rodacker\Cart\Utils\Helper;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class SessionStorage
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\CartBundle\Cart\Storage\Session
 */
class SessionStorage implements CartStorageInterface
{

    /** @var  Session */
    private $session;

    /**
     * @var string
     */
    private $attributeName = Helper::STORAGE_KEY;

    /**
     * SessionStorage constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param string $attributeName
     */
    public function setAttributeName($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @param CartIdentifierInterface $identifier
     *
     * @throws CanNotLoadCartException
     *
     * @return CartInterface
     */
    public function load(CartIdentifierInterface $identifier)
    {
        if ( !$this->has($identifier)) {
            throw new CanNotLoadCartException(
                sprintf(
                    'Can not load cart from session. No entry for attribute name %s found!',
                    $this->attributeName
                )
            );
        }

        return unserialize(
            $this->session->get($this->attributeName)
        );
    }

    /**
     * @param CartInterface $cart
     *
     * @return CartInterface
     */
    public function save(CartInterface $cart)
    {
        $this->session->set($this->attributeName, serialize($cart));
        $this->session->save();

        return $cart;
    }

    /**
     * @param CartIdentifierInterface $identifier
     */
    public function delete(CartIdentifierInterface $identifier)
    {
        if ($this->has($identifier)) {
            $this->session->remove($this->attributeName);
        };
    }

    /**
     * @return bool
     */
    public function has(CartIdentifierInterface $identifier)
    {
        return $this->session->has($this->attributeName);
    }

}