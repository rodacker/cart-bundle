# Symfony Cart Bundle
This bundle implements the [rodacker/cart](https://www.gitlab.com/rodacker/cart) library as Symfony bundle and
offers some additional Symfony specific features.

## Installation

```php
composer require rodacker/cart-bundle
```

to be continued ...
